/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "FlightRecorder.h"

#include "CmdLine.h"
#include "PdComSession.h"
#include "hcs-url/include/headcode/url/url.hpp"

#include <asio/connect.hpp>
#include <asio/ip/tcp.hpp>
#include <asio/signal_set.hpp>
#include <asio/thread_pool.hpp>
#include <chrono>
#include <fstream>
#include <json/reader.h>
#include <libdaemon/dlog.h>
#include <sstream>
#include <streambuf>

using asio::ip::tcp;
using namespace std::chrono_literals;

struct FlightRecorder::impl: std::streambuf {/*{{{*/
    asio::io_context& ctxt;
    asio::thread_pool pool_;
    tcp::socket socket_{ctxt};

    std::weak_ptr<Json::Value> config_;
    std::unique_ptr<PdComSession> session_;

    std::string tx_buf;

    impl(asio::io_context& ctxt) : ctxt{ctxt} {
        do_setup_signals();
        do_load_config();
    }

    // Reimplemented methods from std::streambuf{{{
    int sync() override {
        asio::write(socket_, asio::buffer(tx_buf));
        tx_buf.clear();
        return 0; }

    std::streamsize xsputn(const char* s, std::streamsize n) override {
        tx_buf.append(s, n);
        return n; }

    int overflow(int c) override {
        tx_buf.push_back(c);
        return c; }

    std::streamsize xsgetn(char* s, std::streamsize n) {
        return socket_.read_some(asio::buffer(s, n)); }
    ///////////////////////////////////////////////////////////////////}}}

    void do_setup_signals() {/*{{{*/
        auto ss = std::make_unique<asio::signal_set>(ctxt);
        ss->add(SIGHUP);        // Reload configuration
        ss->add(SIGINT);        // Terminate
        ss->add(SIGTERM);       // Terminate
        ss->add(SIGQUIT);       // Terminate

        ss->async_wait([=, ss = std::move(ss)](
                const asio::error_code& ec, int signal_number) {
            if (!ec and signal_number == SIGHUP) {
                daemon_log(LOG_INFO, "Reload config");

                do_setup_signals();
                do_load_config();
            } else {
                daemon_log(LOG_INFO, "Exiting...");
                pool_.join();
                ctxt.stop();
            }
        });
    }/*}}}*/

    void do_load_config() {/*{{{*/
        // TODO use async::async_read() for this
        // see https://stackoverflow.com/questions/14001387/how-to-use-asio-with-device-files

        try {
            auto config = std::make_shared<Json::Value>();
            auto conf_file = CmdLine::get()->conf_file();
            std::ifstream{conf_file} >> *config;
            daemon_log(LOG_DEBUG, "Loaded JSON configuration file: %s",
                       conf_file.c_str());
            config_ = config;
            do_resolve(config);
        } catch (std::exception& e) {
            daemon_log(LOG_ERR, "Error loading JSON configuration file: %s",
                       e.what());
        }
    }/*}}}*/

    void do_resolve(std::shared_ptr<Json::Value> config) {/*{{{*/
        auto get_server = [](std::shared_ptr<Json::Value> cfg) {
            return (*cfg)["address"].asString(); };

        auto server = get_server(config);

        if (config_.lock() == config and session_
            and get_server(session_->config()) == server) {

            daemon_log(LOG_DEBUG, "Reusing existing session");
            session_->setConfig(config);
            return;
        }

        auto url = headcode::url::URL{server};
        auto host = std::string{url.GetHost()};

        auto service = url.GetPort();
        if (service.empty()) {
            static const std::unordered_map<std::string, std::string>
                portmap{{"msr", "2345"}, {"msrs", "4523"}};

            auto it = portmap.find(std::string{url.GetScheme()});

            if (it == portmap.end())
                it = portmap.find("msr");

            service = it->second;
        }

        auto resolver = std::make_unique<tcp::resolver>(ctxt);
        resolver->async_resolve(
            host, service,
            [=, resolver = std::move(resolver)] (
                const asio::error_code& ec,
                tcp::resolver::results_type results
                ) {
            if (ec) {
                daemon_log(LOG_ERR, "Failed to resolve %s: %s",
                           host.c_str(), ec.message().c_str());
                do_sleep(config);
            } else {
                std::ostringstream ss;
                for (auto i = results.begin(); i != results.end(); ++i) {
                    if (i != results.begin())
                        ss << ", ";
                    ss << i->endpoint();
                }

                daemon_log(LOG_DEBUG, "resolve: %s", ss.str().c_str());
                do_connect(config, results, host);
            }
        });
    }/*}}}*/

    void do_connect(std::shared_ptr<Json::Value> config, /*{{{*/
                    tcp::resolver::results_type endpoints,
                    std::string host) {

        if (config != config_.lock()
            or (session_ and session_->config() == config))
            return;

        daemon_log(LOG_INFO, "Connecting to %s",
                   host.c_str());

        asio::async_connect(
            socket_, endpoints,
            [=](const asio::error_code& ec,
                const tcp::endpoint& /*endpoint*/) {
                if (ec) {
                    daemon_log(LOG_WARNING, "Could not connect to %s: %s",
                               host.c_str(), ec.message().c_str());
                    do_sleep(config);
                } else {
                    daemon_log(LOG_INFO, "Connected to %s",
                               host.c_str());
                    tx_buf.clear();
                    session_ = std::make_unique<PdComSession>(this, host);
                    session_->setConfig(config);
                    do_wait_read();
                }
            });
    }/*}}}*/

    void do_wait_read() {/*{{{*/
        asio::steady_timer timer{ctxt, 5s};
        timer.async_wait(
            [=] (const asio::error_code& ec) {
            if (!ec) {
                daemon_log(LOG_ERR,
                           "Timeout waiting on client. Aborting session...");
                socket_.close();
            }
        });

        socket_.async_wait(
            tcp::socket::wait_read,
            [=, timer=std::move(timer)](const asio::error_code& ec) {
                if (ec)
                    daemon_log(LOG_ERR, "Error waiting on socket: %s",
                               ec.message().c_str());
                else if (do_read())
                    return do_wait_read();

                do_sleep(session_->config());
                session_.reset();
            });
    }/*}}}*/

    bool do_read() {/*{{{*/
        try {
            session_->asyncData();
            return true;
        } catch (const asio::system_error& e) {
            if (e.code() == asio::error::eof)
                daemon_log(LOG_INFO, "Session closed by server");
            else
                daemon_log(LOG_ERR,
                           "System error while reading from socket: %s",
                           e.what());
        } catch (const std::exception& e) {
            daemon_log(LOG_ERR, "Protocol error: %s",
                       e.what());
        }

        return false;
    }/*}}}*/

    void do_sleep(std::shared_ptr<Json::Value> config) {/*{{{*/
        auto timer = std::make_unique<asio::steady_timer>(ctxt);
        timer->expires_after(1s);
        timer->async_wait(
            [=, timer = std::move(timer)] ( const asio::error_code& ec) {
                if (!ec)
                    do_resolve(config);
            });
    }/*}}}*/
};/*}}}*/

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
FlightRecorder::FlightRecorder(asio::io_context& ctxt)
    : p_impl{std::make_unique<impl>(ctxt)} {}
FlightRecorder::~FlightRecorder() {}
