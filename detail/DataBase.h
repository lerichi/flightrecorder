/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <memory>

namespace PdCom {
    class Process;
    struct Message;
    class Variable;
}

namespace Json {
    class Value;
}

namespace detail { namespace db {

struct File {
    virtual ~File() {}

    static std::unique_ptr<File> create(
        PdCom::Process*, const Json::Value&);

    virtual bool activate() = 0;
    virtual bool prune() = 0;
    virtual void processMessage(const PdCom::Message&) = 0;
    virtual void add(PdCom::Variable, double sample_time) = 0;

    protected:
    File() {}
};

}}
