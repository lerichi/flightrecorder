/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/
 
#pragma once

#include <string>
#include <sstream>
#include <list>

#define _STR(x) #x
#define STR(x) _STR(x)

namespace util {

inline std::list<std::string> split(std::string s, char delim) {
    std::stringstream ss{s};
    std::list<std::string> list;
    std::string segment;

    while (std::getline(ss, segment, delim))
        list.push_back(segment);

    return list;
}

struct stringstream: std::streambuf, std::ostream {
    using std::streambuf::int_type;

    explicit stringstream(std::string& str) :
        std::ostream{this}, m_str{str} {}

    int_type overflow(int_type c) override {
        if (c != EOF)
            m_str.push_back(c);

        return c;
    }

    std::streamsize xsputn(const char* s, std::streamsize n) override {
        m_str.append(s, n);
        return n;
    }

    private:
    std::string& m_str;
};

template <typename T>
auto to_hex_str(const T& val)
{
    std::ostringstream os;
    os << std::hex << val;
    return os.str();
}

struct exception: std::runtime_error {
    exception(const std::string& arg, const char* file, int line)
        : std::runtime_error{arg}, p_file{file}, p_line{line} {}
    const char* file() const { return p_file; }
    int line() const { return p_line; }

    private:
    const char * const p_file;
    const int p_line;
};
#define THROW_EXCEPTION(arg) throw util::exception((arg), __FILE__, __LINE__)

template <typename F>
struct Final_action {
    explicit Final_action(F f): clean{f} {}
    ~Final_action() { clean(); }

    Final_action(const Final_action&) = delete;
    Final_action& operator=(const Final_action&) = delete;

    private:
    const F clean;
};

template <typename F>
Final_action<F> finally(F f) { return Final_action<F>{f}; }

}
