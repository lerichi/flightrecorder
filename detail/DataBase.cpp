/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "DataBase.h"
#include "util.h"

#include <H5Cpp.h>
#include <algorithm>    // all_of(), max(), for_each()
#include <array>
#include <chrono>
#include <climits>      // PATH_MAX
#include <cmath>
#include <ctime>
#include <fstream>
#include <json/reader.h>
#include <json/writer.h>
#include <libdaemon/dlog.h>
#include <list>
#include <map>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>
#include <string>
#include <unordered_map>

using namespace detail;
using namespace std::chrono;

namespace {

struct hdf5_db;

static const auto scalar = H5::DataSpace{H5S_SCALAR};
static const auto utf8_str = []() {
    auto str = H5::StrType{H5::PredType::C_S1, H5T_VARIABLE};
    str.setCset(H5T_CSET_UTF8);
    return H5::StrType{str}; }();
static const auto unlimited_space = []() {
    static const hsize_t zero{0};
    static const hsize_t maxdims{H5S_UNLIMITED};
    return H5::DataSpace{1, &zero, &maxdims}; }();
static const auto lcpl = []() {
    auto lcpl = H5::LinkCreatPropList::DEFAULT;
    lcpl.setCreateIntermediateGroup(true);
    return lcpl; }();

struct logentry_t {
    uint64_t time;
    int8_t level;
    std::string path;
    int index;
    std::string text;
};

static const auto logentry_dtype = []() {/*{{{*/
        auto loglevel_type = H5::EnumType{H5::PredType::NATIVE_INT8};
#       define ADD_ENUM(_enum, name) do { \
                auto level = int8_t(PdCom::LogLevel::name); \
                (_enum).insert(#name, &level); \
        } while (0)
        
        ADD_ENUM(loglevel_type, Reset    );
        ADD_ENUM(loglevel_type, Emergency);
        ADD_ENUM(loglevel_type, Alert    );
        ADD_ENUM(loglevel_type, Critical );
        ADD_ENUM(loglevel_type, Error    );
        ADD_ENUM(loglevel_type, Warn     );
        ADD_ENUM(loglevel_type, Info     );
        ADD_ENUM(loglevel_type, Debug    );
        ADD_ENUM(loglevel_type, Trace    );

        auto dtype = H5::CompType{sizeof(logentry_t)};
        dtype.insertMember( "Time",
            HOFFSET(logentry_t, time), H5::PredType::NATIVE_UINT64);
        dtype.insertMember( "LogLevel",
            HOFFSET(logentry_t, level), loglevel_type);
        dtype.insertMember( "Path",
            HOFFSET(logentry_t, path), utf8_str);
        dtype.insertMember( "Index",
            HOFFSET(logentry_t, index), H5::PredType::NATIVE_INT);
        dtype.insertMember( "Text",
            HOFFSET(logentry_t, text), utf8_str);

        return H5::DataType{dtype}; }();/*}}}*/

static H5::DSetCreatPropList dcpl(hsize_t chunk_count) {
    auto dcpl = H5::DSetCreatPropList{H5::DSetCreatPropList::DEFAULT};
    dcpl.setChunk(1, &chunk_count);
    return dcpl; }

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct h5dset: H5::DataSet {/*{{{*/
    h5dset(H5::H5File* db,
           const H5std_string& name,
           const H5::DataType& _dtype,
           hsize_t chunk_size) :
        H5::DataSet{db->createDataSet(
                name, _dtype, unlimited_space, dcpl(chunk_size),
                H5::DSetAccPropList::DEFAULT, lcpl)} {}

    void append(const void *data) {
        auto last_index = extend();

        /* Select a hyperslab at the end */
        static const hsize_t mdims{1};
        auto file_space = DataSet::getSpace();
        file_space.selectHyperslab(H5S_SELECT_SET, &mdims, &last_index);

        static const H5::DataSpace mem_space{1, &mdims};
        DataSet::write(data, DataSet::getDataType(), mem_space, file_space);
    }

    private: ////////////////////////////////////////////////////////////////
    hsize_t extend() {
        hsize_t count;
        DataSet::getSpace().getSimpleExtentDims(&count);
        ++count;
        DataSet::extend(&count);
        return count - 1;
    }
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct MySubscription: PdCom::Subscription {/*{{{*/
    hdf5_db* const db;
    PdCom::Subscriber * const subscriber;

    const std::string path;

    MySubscription(
        hdf5_db* db, PdCom::Subscriber* _s,
        const PdCom::Variable& var, std::string _p)
        : PdCom::Subscription{*_s, var},
        db{db}, subscriber{_s}, path{_p} {}

    void reset() { m_dset.reset(); }

    void newValues();

    private:

    std::unique_ptr<h5dset> m_dset;
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct Group: PdCom::Subscriber {/*{{{*/
    hdf5_db* const db;

    Group(hdf5_db* db, double ts)
        : Subscriber{PdCom::Transmission::fromDouble(ts)}, db{db} {
            m_name = (
                std::string{"TimeVector_"}
                + std::to_string(ts) + '_'
                + util::to_hex_str(reinterpret_cast<void*>(this))); }

    const std::string& timeVectorName() const { return m_name; }

    void add(const PdCom::Variable& var, std::string path) {
        m_subscriptionList.emplace_back(db, this, var, path); }

    bool isReady() const {
        constexpr auto Active = PdCom::Subscription::State::Active;
        return std::all_of(
            m_subscriptionList.begin(), m_subscriptionList.end(),
            [&](auto& s) { return s.getState() == Active; }); }

    void reset() {
        std::for_each(m_subscriptionList.begin(), m_subscriptionList.end(),
                      [](auto& s) { s.reset(); });
        m_dset.reset();
    }

    // Reimplemented from PdCom::Subscriber
    void stateChanged(const PdCom::Subscription&) final {}
    void newValues(std::chrono::nanoseconds) final;

    private:
    std::string m_name;
    std::unique_ptr<h5dset> m_dset;
    std::list<MySubscription> m_subscriptionList;
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct ProcessMessage: h5dset {/*{{{*/
    ProcessMessage(H5::H5File* db) :
        h5dset{db, "/ProcessMessageTable", logentry_dtype, 1} {}

    void append(PdCom::Message m) {
        auto data = logentry_t{
            uint64_t(m.time.count()), int8_t(m.level),
                m.path, m.index, m.text };
        h5dset::append(&data);
    }
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct hdf5_db: db::File {/*{{{*/
    PdCom::Process * const process;

    ////////////////////////////////////////////////////////////////////////
    hdf5_db(PdCom::Process* _p, const Json::Value& _c) :
        process{_p}, config{_c} {
            max_size = config.get("size limit", 1000000U).asUInt64();
            duration_limit = seconds{
                config.get("time limit", 24*60*60).asUInt()};
            if (duration_limit != steady_clock::duration::zero())
                end_time = steady_clock::now() + duration_limit;

            util::stringstream{config_str} << _c << "\n\n";
        }

    ////////////////////////////////////////////////////////////////////////
    bool prune() final {
        if (db and (
                (max_size and db->getFileSize() > max_size)
                or (duration_limit.count()
                    and steady_clock::now() > end_time))) {
            end_time += duration_limit;

            std::for_each(groupList.begin(), groupList.end(),
                          [](auto& g) { g.reset(); });

            m_processMessage.reset();

            db.reset();

            return true;
        }

        return false;
    }

    ////////////////////////////////////////////////////////////////////////
    void processMessage(const PdCom::Message& m) final {
        if (not m_processMessage)
            m_processMessage = std::make_unique<ProcessMessage>(getDB());

        m_processMessage->append(m);
    }

    ////////////////////////////////////////////////////////////////////////
    void add(PdCom::Variable var, double interval) final {
        // First need to put all variables into a map, because the
        // paths may collide.
        variableMap.emplace(var.getPath(), std::make_pair(var, interval));
    }

    ////////////////////////////////////////////////////////////////////////
    bool activate() {
        for (auto it = variableMap.begin(); it != variableMap.end(); ++it) {

            auto& var = it->second.first;
            auto interval = it->second.second;

            auto taskId = var.getTaskId();

            auto tsample = var.getSampleTime().count();
            auto decimation = tsample and interval
                ? std::max<unsigned>(1U, std::round(interval / tsample))
                : 0U;

            auto* group = decimation
                ? groupMap[taskId][decimation]
                : nullptr;

            if (not group) {
                groupList.emplace_back(this, decimation * tsample);
                group = &groupList.back();

                if (decimation)
                    groupMap[taskId][decimation] = group;
            }

            auto path = var.getPath();
            auto it1 = std::next(it);
            if (it1 != variableMap.end()
                and it1->first.size() > path.size()
                and it1->first[path.size()] == '/'
                and it1->first.compare(0, path.size(), path) == 0) {

                daemon_log(LOG_DEBUG, "Renaming due to name collision: %s",
                           path.c_str());
                path.push_back('_');
            }

            daemon_log(LOG_DEBUG, "Adding signal %s",
                       path.c_str());
            group->add(var, path);
        }

        variableMap.clear();

        return std::all_of(groupList.begin(), groupList.end(),
                           [](auto& g) { return g.isReady(); });
    }

    ////////////////////////////////////////////////////////////////////////
    H5::H5File* getDB() {/*{{{*/

        if (!db) {
            auto t = time(NULL);

            struct tm tm;
            gmtime_r(&t, &tm);

            std::array<char, PATH_MAX> name_buf;
            auto dbfile = config["path"].asString();
            if (dbfile.empty())
                dbfile = std::string("/var/spool/flightrecorder/")
                    + process->name() + "-%Y%m%d-%H%M%S.h5";
            strftime(name_buf.data(), name_buf.size(), dbfile.c_str(), &tm);

            hsize_t user_len = 512;     // Minimum is 512 bytes
            while (user_len < config_str.size())
                user_len <<= 1;

            // Create space at file start for user data where the
            // configuration file is stored
            H5::FileCreatPropList fcpl{H5::FileCreatPropList::DEFAULT};
            fcpl.setUserblock(user_len);

            db = std::make_unique<H5::H5File>(
                name_buf.data(), H5F_ACC_TRUNC, fcpl);

            // Write json configuration to file's user space
            std::ofstream{name_buf.data(), std::ios::binary} << config_str;

            db->createAttribute("name", utf8_str, scalar)
                .write(utf8_str, process->name());
            db->createAttribute("version", utf8_str, scalar)
                .write(utf8_str, process->version());

            start_time = steady_clock::now();

            uint64_t _time = duration_cast<seconds>(
                system_clock::now().time_since_epoch())
                .count();

            db->createAttribute(
                "start time", H5::PredType::NATIVE_UINT64, scalar)
                .write(H5::PredType::NATIVE_UINT64, &_time);

            if (!previous_db.empty())
                db->createAttribute("previous db", utf8_str, scalar)
                    .write(utf8_str, previous_db);
            previous_db.assign(name_buf.data());
        }

        return db.get();
    }/*}}}*/

    ////////////////////////////////////////////////////////////////////////
    const Json::Value& getConfig() const { return config; }

    ////////////////////////////////////////////////////////////////////////
    hsize_t chunk_size(double tsample) const {
        auto chunk_time = config.get("chunk time", 10.0).asDouble();

        if (chunk_time <= 0.0)
            THROW_EXCEPTION("Invalid chunk time specification");

        return tsample
            ? std::max<hsize_t>(std::round(chunk_time / tsample), 1U)
            : 1U;
    }

    ////////////////////////////////////////////////////////////////////////
    hid_t getId() const { return db->getId(); }

    private: ///////////////////////////////////////////////////////////////
    const Json::Value& config;

    std::unique_ptr<H5::H5File> db;
    steady_clock::time_point start_time;

    hsize_t max_size;
    seconds duration_limit;
    steady_clock::time_point end_time;

    std::string config_str;
    std::string previous_db;

    std::map<std::string, std::pair<PdCom::Variable, double>> variableMap;

    std::unique_ptr<ProcessMessage> m_processMessage;

    std::list<Group> groupList;

    using Decimation_T = unsigned;
    using TaskId_T = decltype(PdCom::Variable{}.getTaskId());
    using SubscriberMap = std::unordered_map<Decimation_T, Group*>;
    using TaskSubscriberMap = std::unordered_map<TaskId_T, SubscriberMap>;
    TaskSubscriberMap groupMap;
};/*}}}*/

}       // namespace

////////////////////////////////////////////////////////////////////////////
void MySubscription::newValues() {
    if (not m_dset) {/*{{{*/
        using PdComType = PdCom::TypeInfo::DataType;
        using H5Type = H5::PredType;
        static const std::unordered_map<PdComType, H5Type> typemap = {
            {PdCom::TypeInfo::boolean_T, H5::PredType::NATIVE_HBOOL},
            {PdCom::TypeInfo::uint8_T,   H5::PredType::NATIVE_UINT8},
            {PdCom::TypeInfo::int8_T,    H5::PredType::NATIVE_INT8},
            {PdCom::TypeInfo::uint16_T,  H5::PredType::NATIVE_UINT16},
            {PdCom::TypeInfo::int16_T,   H5::PredType::NATIVE_INT16},
            {PdCom::TypeInfo::uint32_T,  H5::PredType::NATIVE_UINT32},
            {PdCom::TypeInfo::int32_T,   H5::PredType::NATIVE_INT32},
            {PdCom::TypeInfo::uint64_T,  H5::PredType::NATIVE_UINT64},
            {PdCom::TypeInfo::int64_T,   H5::PredType::NATIVE_INT64},
            {PdCom::TypeInfo::double_T,  H5::PredType::NATIVE_DOUBLE},
            {PdCom::TypeInfo::single_T,  H5::PredType::NATIVE_FLOAT},
            {PdCom::TypeInfo::char_T,    H5::PredType::NATIVE_CHAR},
        };
        static_assert(PdCom::TypeInfo::DataTypeEnd == 12);

        auto variable = getVariable();

        /* Variable shape and type */
        auto dtype = H5::DataType{typemap.at(variable.getTypeInfo().type)};
        auto sizeInfo = variable.getSizeInfo();
        if (!sizeInfo.isScalar()) {
            std::vector<hsize_t> dims(sizeInfo.begin(), sizeInfo.end());
            dtype = H5::ArrayType{dtype, (int)dims.size(), dims.data()};
        }

        auto tsample = subscriber->getTransmission().getInterval();
        m_dset = std::make_unique<h5dset>(
            db->getDB(), path, dtype, db->chunk_size(tsample));

        // Add PdCom::Variable() attributes
        m_dset->createAttribute("Path", utf8_str, scalar)
            .write(utf8_str, variable.getPath());

        std::string alias{variable.getAlias()};
        if (!alias.empty())
            m_dset->createAttribute("Alias", utf8_str, scalar)
                .write(utf8_str, alias);

        int taskId{variable.getTaskId()};
        m_dset->createAttribute("TaskId", H5::PredType::NATIVE_INT, scalar)
            .write(H5::PredType::NATIVE_INT, &taskId);

        bool isWriteable{variable.isWriteable()};
        if (isWriteable)
            m_dset->createAttribute(
                "Writeable", H5::PredType::NATIVE_HBOOL, scalar)
                .write(H5::PredType::NATIVE_HBOOL, &isWriteable);

        // Add time vector attribute
        hobj_ref_t time_ref;
        auto& time_vector_name =
            static_cast<Group*>(subscriber)->timeVectorName();
        if (H5Rcreate(&time_ref, db->getId(),
                      time_vector_name.c_str(), H5R_OBJECT, -1) < 0)
            THROW_EXCEPTION(
                std::string{"Could not create reference to /"}
                + time_vector_name);
        m_dset->createAttribute("Time", H5::PredType::STD_REF_OBJ, scalar)
            .write(H5::PredType::STD_REF_OBJ, &time_ref);
    }/*}}}*/

    m_dset->append(getData());
}

////////////////////////////////////////////////////////////////////////////
void Group::newValues(std::chrono::nanoseconds ns) {
    if (not m_dset) {/*{{{*/
        auto tsample = getTransmission().getInterval();
        static const auto dtype = H5::PredType::NATIVE_UINT64;
        m_dset = std::make_unique<h5dset>(
            db->getDB(), m_name, dtype, db->chunk_size(tsample));
    }/*}}}*/

    uint64_t time = ns.count();
    m_dset->append(&time);

    std::for_each(m_subscriptionList.begin(), m_subscriptionList.end(),
                  [](auto& s) { s.newValues(); });
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
std::unique_ptr<db::File> db::File::create(
    PdCom::Process* _p, const Json::Value& _c) {
    return std::make_unique<hdf5_db>(_p, _c);
}
