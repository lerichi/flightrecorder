/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TcpServer.h"

#include <thread>

//#include <asio.hpp>
#include <asio/ip/tcp.hpp>

#include <libdaemon/dlog.h>

#include "flightrecorder.capnp.h"

using asio::buffer;
using asio::ip::tcp;

typedef unsigned int uint;
//https://groups.google.com/g/capnproto/c/oT-4_sOwAWw 
// kj::AsyncIoStream https://stackoverflow.com/questions/33113199/what-is-a-good-way-to-secure-capn-proto-rpc-network-traffic/33114789#33114789
//restbed
// QtEventPort https://github.com/capnproto/capnproto/pull/253/commits/f5d0242644b3c7097a73f751ff27bbd9ebbf4eb8

struct DBImpl final: DB::Server {
    kj::Promise<void> list(ListContext context) override {
        context.getResults().setValue({"a", "b"});
        return kj::READY_NOW;
    }
};

/////////////////////////////////////////////////////////////////////////////
// TcpServer::impl definition
/////////////////////////////////////////////////////////////////////////////
struct TcpServer::impl {
    impl(PdComSession* pdcom, const Json::Value& config)
        : pdcom_{pdcom} {
            (void)config;
            thread = std::thread(&impl::run, this);
        }
    ~impl() {
        daemon_log(LOG_DEBUG, "%s", __func__);
        ctxt.stop();
        thread.join();
    }

    void run() {
        daemon_log(LOG_DEBUG, "XXXXXXXXXXXXXXXXX 1 %lu", pthread_self());

        auto listen_endpoint = *tcp::resolver(ctxt).resolve(
            "", "4300", tcp::resolver::passive);

        tcp::acceptor acceptor(ctxt, listen_endpoint);

        listen(acceptor);

        ctxt.run();
    }

    void listen(tcp::acceptor& acceptor) {
        acceptor.async_accept(
            [this, &acceptor](const std::error_code error, tcp::socket client) {
                if (!error) {
                    daemon_log(LOG_DEBUG, "new client");
                } else {
                }

                listen(acceptor);
            });
    }

    private:
    const PdComSession* pdcom_;

    std::thread thread;

    asio::io_context ctxt;
};

/////////////////////////////////////////////////////////////////////////////
// Public interface definition
/////////////////////////////////////////////////////////////////////////////
TcpServer::TcpServer(PdComSession* pdcom, const Json::Value& config)
    : p_impl{std::make_unique<impl>(pdcom, config)} {}
TcpServer::~TcpServer() {}
