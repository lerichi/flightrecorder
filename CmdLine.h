/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/
 
#pragma once

#include <memory>
#include <string>

struct CmdLine {
    ~CmdLine();

    static void parse(int argc, char** argv);
    static CmdLine* get();

    bool kill() const;
    bool reload() const;
    bool daemon() const;
    bool verbose() const;
    std::string pid_file() const;
    std::string conf_file() const;

    private:
    CmdLine();

    struct impl;
    const std::unique_ptr<impl> p_impl;
};
