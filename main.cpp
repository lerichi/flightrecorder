/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "CmdLine.h"
#include "FlightRecorder.h"
#include "detail/util.h"

#include <asio/io_context.hpp>
#include <cstring>
#include <libdaemon/dfork.h>
#include <libdaemon/dlog.h>
#include <libdaemon/dpid.h>
#include <csignal>

bool become_daemon() {/*{{{*/
    /* Check that the daemon is not rung twice a the same time */
    pid_t pid = daemon_pid_file_is_running();
    if (pid >= 0)
        throw std::runtime_error{
            std::string{"Daemon already running on PID "}
            + std::to_string(pid)};

    /* Reset signal handlers */
    if (daemon_reset_sigs(-1) < 0)
        throw std::system_error{errno, std::system_category(),
            "Failed to reset all signal handlers"};

    /* Unblock signals */
    if (daemon_unblock_sigs(-1) < 0)
        throw std::system_error{errno, std::system_category(),
            "Failed to unblock all signals"};

    if (daemon_retval_init() < 0)
        throw std::system_error{errno, std::system_category(),
            "Failed to create return value pipe"};
    auto _at_end = util::finally(daemon_retval_done);

    /* Do the fork */
    pid = daemon_fork();
    if (pid < 0)
        throw std::system_error{errno, std::system_category(),
            "Could not fork()"};

    if (pid) { /* The parent */
        /* Wait for 20 seconds for the return value passed from the daemon
         * process */
        auto ret = daemon_retval_wait(20);

        if (ret < 0)
            throw std::system_error{errno, std::system_category(),
                "Could not receive return value from daemon process"};

        if (ret > 0)
            throw std::runtime_error{"Error from daemon. See logfile"};

        return true;
    } 

    /* The daemon */

    /* Close FDs */
    if (daemon_close_all(-1) < 0) {
        daemon_log(LOG_ERR, "Failed to close all file descriptors: %s",
                   strerror(errno));

        /* Send the error condition to the parent process */
        daemon_retval_send(errno);
        return true;
    }

    /* Create the PID file */
    if (daemon_pid_file_create() < 0) {
        daemon_log(LOG_ERR, "Could not create PID file. Continuing...");
        daemon_retval_send(errno);
    }

    /* Register function to clean up PID file at program termination */
    atexit([] { daemon_pid_file_remove(); });

    /* Send OK to parent process */
    daemon_retval_send(0);

    daemon_log(LOG_INFO, "Sucessfully started");

    return false;
}/*}}}*/

int main(int argc, char **argv)
{
    CmdLine::parse(argc, argv);

    /* Set indentification string for the daemon for both syslog
     * and PID file */
    daemon_pid_file_ident
        = daemon_log_ident
        = daemon_ident_from_argv0(argv[0]);

    /* Set pid file name generator */
    daemon_pid_file_proc = []() -> const char* {
        static std::string pid_file{CmdLine::get()->pid_file()};
        return pid_file.empty()
            ? daemon_pid_file_proc_default() : pid_file.c_str(); };

    if (CmdLine::get()->verbose())
        daemon_set_verbosity(LOG_DEBUG);

    if (CmdLine::get()->kill()) {
        if (not daemon_pid_file_kill_wait(SIGTERM, 5))
            return 0;

        daemon_log(LOG_WARNING, "Failed to kill daemon: %s",
                   strerror(errno));

        return errno;
    }

    if (CmdLine::get()->reload()) {
        if (not daemon_pid_file_kill(SIGHUP))
            return 0;

        daemon_log(LOG_WARNING, "Failed to reload daemon: %s",
                   strerror(errno));

        return errno;
    }

    try {
        if (CmdLine::get()->daemon() and become_daemon())
            return 0;

        asio::io_context ctxt;

        FlightRecorder fr{ctxt};

        ctxt.run();
        daemon_log(LOG_INFO, "Finished");
        return 0;
    } catch (std::system_error& e) {
        daemon_log(LOG_ERR, "%s", e.what());
        return e.code().value();
    } catch (std::exception& e) {
        daemon_log(LOG_ERR, "%s", e.what());
        return -1;
    }
}
