/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "PdComSession.h"
#include "tcpserver/TcpServer.h"

#include "detail/DataBase.h"

#include <cmath>        // std::round()
#include <json/value.h>
#include <libdaemon/dlog.h>
#include <libdaemon/dlog.h>
#include <map>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <streambuf>
#include <unordered_map>

using namespace detail;

/////////////////////////////////////////////////////////////////////////////
// PdComSession::impl definition
/////////////////////////////////////////////////////////////////////////////
struct PdComSession::impl: PdCom::Process, PdCom::MessageManagerBase {
    std::streambuf * const sb_;
    std::string host_;

    std::shared_ptr<Json::Value> config_;
    std::unique_ptr<TcpServer> server_;

    bool messages_active_{false};
    bool connected_{false};
    unsigned find_count_{0};

    using SubscriptionRate_T = double;
    std::unordered_map<std::string, SubscriptionRate_T> discoveryMap_;

    std::unique_ptr<db::File> active_db_, pending_db_;

    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    impl(std::streambuf* sb, std::string host) : sb_{sb}, host_{host} { }

    ///////////////////////////////////////////////////////////////////////
    void setConfig(std::shared_ptr<Json::Value> config) {/*{{{*/
        config_ = config;

        server_ = std::make_unique<TcpServer>(nullptr, (*config)["server"]);

        if (connected_)
            subscribe();
    }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    const Json::Value& config(const char* key) const {/*{{{*/
        return config_->operator[](key); }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    void asyncData() {/*{{{*/
        PdCom::Process::asyncData();

        if (not find_count_ and pending_db_ and pending_db_->activate()) {

            messages_active_ = false;
            auto& messages = config("messages");
            if (not messages or messages.asBool()) {
                daemon_log(LOG_DEBUG, "Fetching active messages");
                setMessageManager(this);
                PdCom::MessageManagerBase::activeMessages();
            }

            active_db_ = std::move(pending_db_);
        }
    }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    void subscribe() {/*{{{*/
        pending_db_ = db::File::create(this, config("db"));

        auto var = config("variable");
        auto keys = var.getMemberNames();

        // First set find_count_ because findReply() could be called within
        // PdCom::Process::find()
        find_count_ += keys.size();

        for (auto& _k: keys) {
            daemon_log(LOG_DEBUG, "Searching for variable %s",
                       _k.c_str());
            discoveryMap_[_k] = var[_k].asDouble();
            find(_k);
        }
    }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    int read(char* buf, int count) final {/*{{{*/
        return sb_->sgetn(buf, count); }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    void write(const char* buf, size_t count) final {/*{{{*/
        sb_->sputn(buf, count); }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    void flush() final {/*{{{*/
        sb_->pubsync(); }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    void connected() final {/*{{{*/
        daemon_log(LOG_DEBUG, "Protocol initialization complete");
        connected_ = true;
        subscribe();
    }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    std::string applicationName() const final {/*{{{*/
        return "flightrecorder"; }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Process
    std::string hostname() const final {/*{{{*/
        return host_; }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::Subscriber
    void findReply(const PdCom::Variable& _v) final {/*{{{*/
        --find_count_;

        if (_v.empty())
            return;

        auto it = discoveryMap_.find(_v.getPath());

        if (it != discoveryMap_.end()) {
            daemon_log(LOG_DEBUG, "Found variable %s",
                       it->first.c_str());
            pending_db_->add(_v, it->second);
            discoveryMap_.erase(it);
        }

        if (not find_count_) {
            for (auto& i: discoveryMap_) {
                daemon_log(LOG_WARNING, "Could not find variable %s",
                           i.first.c_str());
            }

            discoveryMap_.clear();
        }
    }/*}}}*/


    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::MessageManagerBase
    void activeMessagesReply(/*{{{*/
        std::vector<PdCom::Message> messageList) final {

        messages_active_ = true;

        for (auto& m: messageList)
            processMessage(m);
    }/*}}}*/

    ///////////////////////////////////////////////////////////////////////
    // Reimplemented functions from PdCom::MessageManagerBase
    void processMessage(PdCom::Message m) final {
        if (messages_active_)
            active_db_->processMessage(m);
    }

};

/////////////////////////////////////////////////////////////////////////////
// Public interface definition
/////////////////////////////////////////////////////////////////////////////
PdComSession::PdComSession(std::streambuf* sb, std::string host)
    : p_impl{std::make_unique<impl>(sb, host)} {}
PdComSession::~PdComSession() {}
std::shared_ptr<Json::Value> PdComSession::config() const {
    return p_impl->config_; }
void PdComSession::setConfig(std::shared_ptr<Json::Value> config) {
    p_impl->setConfig(config); }
void PdComSession::asyncData() { p_impl->asyncData(); }
