/****************************************************************************
 * This file is part of FlightRecorder.
 *
 * Copyright (C) 2023 Richard Hacker
 *
 * FlightRecorder is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * FlightRecorder is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FlightRecorder. If not, see <https://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "CmdLine.h"
#include "flightrecorder_version.h"
#include "detail/util.h"        // STR()

#include <tclap/CmdLine.h>

struct CmdLine::impl {
    TCLAP::ValueArg<std::string> conf_file{
        "c", "config", "Location of JSON configuration file",
            false, std::string{"/etc/flightrecorder.conf"},
            "path"};
    TCLAP::ValueArg<std::string> pid_file{
        "p", "pid", "Custom location of daemon PID file",
            false, std::string{}, "path"};

    TCLAP::SwitchArg daemon{"d", "daemon", "Become daemon"};
    TCLAP::SwitchArg kill{"k", "kill", "Kill daemon process"};
    TCLAP::SwitchArg reload{"r", "reload", "Reload config file"};
    TCLAP::SwitchArg verbose{
        "v", "verbose", "Verbose logging to console"} ;
};

CmdLine::CmdLine() : p_impl{std::make_unique<impl>()} {}
CmdLine::~CmdLine() {}

bool CmdLine::kill() const { return p_impl->kill.getValue(); }
bool CmdLine::reload() const { return p_impl->reload.getValue(); }
bool CmdLine::daemon() const { return p_impl->daemon.getValue(); }
bool CmdLine::verbose() const { return p_impl->verbose.getValue(); }
std::string CmdLine::pid_file() const {
    return p_impl->pid_file.getValue(); }
std::string CmdLine::conf_file() const {
    return p_impl->conf_file.getValue(); }

CmdLine* CmdLine::get() {
    static CmdLine instance;
    return &instance;
}

void CmdLine::parse(int argc, char** argv) {
    auto& impl = get()->p_impl;

    TCLAP::CmdLine cmd{
        "FlightRecorder is a data logging system that connects to PdServ"
            " processes and logs signals, parameters and messages.\n"
            "Copyright (C) 2023 Richard Hacker\n"
            "License: GPLv3",
            ' ', STR(PROJECT_VERSION)};

    cmd.add(impl->daemon);
    cmd.add(impl->kill);
    cmd.add(impl->reload);
    cmd.add(impl->verbose);
    cmd.add(impl->conf_file);
    cmd.add(impl->pid_file);

    cmd.parse(argc, argv);
}
